//请求数据的封装
const request = path =>{
    let url = "https://www.zhengzhicheng.cn/api/public/v1" + path
    return new Promise(function(resolve,reject){
        mpvue.request({
            url: url,
            method:"get",
            success: res=>{
               resolve(res)
            }
        })
    })
}
export default request